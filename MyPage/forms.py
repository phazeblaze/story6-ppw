from django import forms
from .models import StatusModel
class StatusForm(forms.Form):
    status_attrs = {
        'type':'text',
        'class': 'form-control',
        'placeholder':'Update your status here..',
        'rows' : 5,
    }

    status = forms.CharField(label='Status', max_length=300,  required=True, widget=forms.TextInput(attrs=status_attrs))
