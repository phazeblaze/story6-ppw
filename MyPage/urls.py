from django.urls import include, path
from .views import *

app_name = "MyPage"
urlpatterns = [
    path('', index, name='index'),
    path('story8', story8, name='story8')
    
]
