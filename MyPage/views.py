from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .models import StatusModel
from .forms import StatusForm
from datetime import datetime, timedelta

# Create your views here.
def index(request):
    response = {
        "form" : StatusForm(),
        "posts": StatusModel.objects.all()
    }

    if request.method   == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            status = form.cleaned_data['status'] 

            now = datetime.now() + timedelta(hours=7)
            day = now.strftime("%A")
            date = now.strftime("%e")
            month = now.strftime("%B")
            year = now.strftime("%Y")
            hour = now.strftime("%H")
            minute = now.strftime("%M")
            fulldate = f"{day}, {date} {month} {year}"
            time = f"{hour}:{minute} WIB"

            update = StatusModel(status = status, date = fulldate, time = time)
            update.save()
            return redirect("MyPage:index")
            
    return render(request, 'index.html', response)

def story8(request):
    return render(request, 'story8.html')
    
#def update_status(request):
    # if request.method   == 'POST':
    #     form = StatusForm(request.POST)
    #     if form.is_valid:
    #         status = form.cleaned_data['status']

    #         now = datetime.now() + timedelta(hours=7)
    #         day = now.strftime("%A")
    #         date = now.strftime("%e")
    #         month = now.strftime("%B")
    #         year = now.strftime("%Y")
    #         hour = now.strftime("%H")
    #         minute = now.strftime("%M")
    #         fulldate = f"{day}, {date} {month} {year}"
    #         time = f"{hour}:{minute} WIB"

    #         update = StatusModel(status = status, date = fulldate, time = time)
    #         update.save()

    #  return HttpResponseRedirect(reverse('MyPage:index'))


