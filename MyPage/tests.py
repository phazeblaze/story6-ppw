from django.test import TestCase, Client

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.chrome.webdriver import WebDriver

from django.urls import reverse, resolve
import time, datetime, pytz

from .models import StatusModel
from .views import *
from .forms import StatusForm

# C:/UsersASUS/Documents/SEM3/PPW/Story 6/Story6/MyPage/chrome_77_driver/chromedriver.exe
# chrome_options = options
# Create your tests here.

class TestStatus(TestCase):
    def setUp(self):
        self.client = Client()
        options = Options()
        options.add_argument('--headless')
        options.add_argument('no-sandbox')
        options.add_argument('disable-dev-shm-usage')
        # self.browser = webdriver.Chrome(chrome_options=options, executable_path="MyPage/chrome_77_driver/chromedriver")
        self.browser = webdriver.Chrome(chrome_options = options)

    def tearDown(self):
        self.browser.close()

    def test_intro(self):
        self.browser.get('http://trt-story6.herokuapp.com')
        # self.browser = webdriver.Chrome('MyPage/chrome_77_driver/chromedriver')
        # self.browser.get('http://localhost:8000')
        halo = self.browser.find_element_by_class_name('prof').text
        self.assertEquals(halo, "HALO, APA KABAR?")

    def test_title(self):
        self.browser.get('http://trt-story6.herokuapp.com')
        # self.browser = webdriver.Chrome('MyPage/chrome_77_driver/chromedriver')
        # self.browser.get('http://localhost:8000')
        self.assertEquals(self.browser.title, "Timothy Regana Tarigan")

    def test_submission(self):
        self.browser.get('http://trt-story6.herokuapp.com')
        # self.browser = webdriver.Chrome('MyPage/chrome_77_driver/chromedriver')
        # self.browser.get('http://localhost:8000')
        form = self.browser.find_element_by_id("id_status")
        button = self.browser.find_element_by_id("update")
        form.send_keys("testing")
        button.click()
        self.assertIn("testing", self.browser.page_source)

    def test_accordion_one(self):
        self.browser.get('http://trt-story6.herokuapp.com')
        # self.browser = webdriver.Chrome('MyPage/chrome_77_driver/chromedriver')
        # self.browser.get('http://localhost:8000')

        activity = self.browser.find_element_by_id("activity")
        activity.click()
        self.assertIn("Gaming", self.browser.page_source)

    def test_accordion_two(self):
        self.browser.get('http://trt-story6.herokuapp.com')
        # self.browser = webdriver.Chrome('MyPage/chrome_77_driver/chromedriver')
        # self.browser.get('http://localhost:8000')

        orgs = self.browser.find_element_by_id("orgs")
        orgs.click()
        self.assertIn("OSIS", self.browser.page_source)

    def test_accordion_three(self):
        self.browser.get('http://trt-story6.herokuapp.com')
        # self.browser = webdriver.Chrome('MyPage/chrome_77_driver/chromedriver')
        # self.browser.get('http://localhost:8000')

        achieve = self.browser.find_element_by_id("achieve")
        achieve.click()
        self.assertIn("uhhhhhhh", self.browser.page_source)

    # def test_initial_search(self):
    #     self.browser.get('http://trt-story6.herokuapp.com/story8')
    #     # self.browser = webdriver.Chrome('MyPage/chrome_77_driver/chromedriver')
    #     # self.browser.get('http://localhost:8000/story8')

    #     self.assertIn("cover", self.browser.page_source)

    # def test_search(self):
    #     self.browser.get('http://trt-story6.herokuapp.com/story8')
    #     # self.browser = webdriver.Chrome('MyPage/chrome_77_driver/chromedriver')
    #     # self.browser.get('http://localhost:8000/story8')

    #     input = self.browser.find_element_by_id("input")
    #     search = self.browser.find_element_by_id("search")

    #     input.send_keys("dogs")
    #     search.click()
    #     self.assertIn("dogs", self.browser.page_source)

    def test_page_exists(self):
        response = self.client.get('')
        self.assertEquals(response.status_code, 200)

    def test_page_dont_exist(self):
        response = self.client.get('/unavailable')
        self.assertEquals(response.status_code, 404)

    def test_model_count(self):
        new_model = StatusModel.objects.create(
            status = 'this is a test',
            date = 'Monday, 4 November 2019',
            time = '23:03 WIB'
        )

        count_models = StatusModel.objects.all().count()
        self.assertEqual(count_models, 1)
    
    def test_if_index_used_the_right_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_form_is_valid(self):
        form = StatusForm({'status':'tes'})
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['status'], 'tes')

    # def test_if_index_used_the_right_template_search(self):
    #     response = Client().get('/')
    #     self.assertTemplateUsed(response, 'story8.html')





    

    

    

