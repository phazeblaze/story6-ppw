from django.db import models

# Create your models here.
class StatusModel(models.Model):
    status = models.CharField(max_length=300)
    date = models.CharField(max_length = 30)
    time = models.CharField(max_length=10)

