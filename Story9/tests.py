from django.test import TestCase, Client

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.chrome.webdriver import WebDriver

from django.urls import reverse, resolve
import time, datetime, pytz

from .views import *

# Create your tests here.
# views masih belom ke-test semua
class TestLogin(TestCase):
    def setUp(self):
        self.client = Client()
        options = Options()
        options.add_argument('--headless')
        options.add_argument('no-sandbox')
        options.add_argument('disable-dev-shm-usage')
        # self.browser = webdriver.Chrome(chrome_options=options, executable_path="MyPage/chrome_77_driver/chromedriver")
        self.browser = webdriver.Chrome(chrome_options = options)

    def tearDown(self):
        self.browser.close()

    def test_intro_story9(self):
        self.browser.get('http://trt-story6.herokuapp.com/story9')
        # self.browser = webdriver.Chrome('MyPage/chrome_77_driver/chromedriver')
        # self.browser.get('http://localhost:8000/story9')
        self.assertIn('<h1 id="hello">Welcome.</h1>',self.browser.page_source)

    def test_title_story9(self):
        self.browser.get('http://trt-story6.herokuapp.com/story9')
        # self.browser = webdriver.Chrome('MyPage/chrome_77_driver/chromedriver')
        # self.browser.get('http://localhost:8000/story9')
        self.assertEquals(self.browser.title, "Login Page - Story 9")

    def test_login(self):
        self.browser.get('http://trt-story6.herokuapp.com/story9')
        # self.browser = webdriver.Chrome('MyPage/chrome_77_driver/chromedriver')
        # self.browser.get('http://localhost:8000/story9')
        username = self.browser.find_element_by_id("id_uname")
        password = self.browser.find_element_by_id("id_pw")
        login = self.browser.find_element_by_id("login")
        username.send_keys("testering")
        password.send_keys("testestes")
        login.click()
        self.assertIn('Hello, testering', self.browser.page_source)
        logout = self.browser.find_element_by_id("logout")
        logout.click()
        self.assertIn('<h1 id="hello">Welcome.</h1>',self.browser.page_source)

    def test_login_fail(self):
        self.browser.get('http://trt-story6.herokuapp.com/story9')
        # self.browser = webdriver.Chrome('MyPage/chrome_77_driver/chromedriver')
        # self.browser.get('http://localhost:8000/story9')
        username = self.browser.find_element_by_id("id_uname")
        password = self.browser.find_element_by_id("id_pw")
        login = self.browser.find_element_by_id("login")
        username.send_keys("test")
        password.send_keys("test")
        login.click()
        self.assertIn('<h1 id="hello">Welcome.</h1>',self.browser.page_source)

    def test_page_exists_story9(self):
        response = self.client.get('')
        self.assertEquals(response.status_code, 200)

    def test_page_dont_exist_story9(self):
        response = self.client.get('/unavailable')
        self.assertEquals(response.status_code, 404)

    
