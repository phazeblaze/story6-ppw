from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required


# Create your views here.
def index(request):
    return render(request, 'story9.html')

def intro(request):
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('/story9/index')
    else:
        form = AuthenticationForm()
    return render(request, 'intro.html', {'form': form})

def lout(request):
    logout(request)
    return redirect('/story9')

# def sup(request):
#     if request.method == "POST":
#         signform = UserCreationForm(request.POST)
#         if signform.is_valid():
#             user = signform.save()
#             return redirect('/story9')
#     else:
#         signform = UserCreationForm()
#     return render(request, 'intro.html', {'signform':signform})

